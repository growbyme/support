**For immediate help, email admin@growbyme.com**

Welcome to GrowByMe support, we are always improving and working on these, please report any issues to help others as well as yourself.

* You can open any links in a new tab to avoid leaving Bitbucket.*

---

## Issues

Submit any issues you may have on our [issue tracker](https://bitbucket.org/growbyme/support/issues?status=new&status=open)

1. Click **Create issue** on the bottom of the [page in the middle](https://bitbucket.org/growbyme/support/issues?status=new&status=open).
2. Fill in a **Title**, please try to be brief but as descriptive as possible. 
3. On the **Description** field, please try to include as many details as possible.
4. For the **Assignee**, you can just skip this field. *You _could_ assign it to a team member but as the team grows its better to let us assign.*
5. The **Kind** dropdown is for you to select the type of issue.  The choices are **bug**, **enhancement**, **proposal**, **task**, so choose wisely.
6. Our policy on **Priority**, is that we may re-prioritize it later, but we honor your choice here for the initial ticket. *default is trivial.*
7. Next you will add any screenshots or other **Attachments** to help us understand the issue.
8. Click **Create Issue**.

---

## Registration

If you have any issues signing up for the site, we have suggestions.

1. Check out our [Wiki](https://bitbucket.org/growbyme/support/wiki/Home)
2. Open an [Issue](https://bitbucket.org/growbyme/support/issues?status=new&status=open) if you notice anything off.

---

## App

The app is a work in progress, expect things to change quickly and often.

1. Instructions to follow.

Thank you for your support, we hope we could help.